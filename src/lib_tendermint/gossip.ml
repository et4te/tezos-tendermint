(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type t =
  | Proposal of Ed25519.Public_key.t * int * int * Value.t option * int
  | Prevote of Ed25519.Public_key.t * int * int * Value.block_hash option * Ed25519.t option
  | Precommit of Ed25519.Public_key.t * int * int * Value.block_hash option * Ed25519.t option

let pp ppf t =
  match t with
  | Proposal (pk, h, r, _v, _vr) ->
     Format.fprintf ppf "[%a] Proposal [%d:%d]" Ed25519.Public_key.pp pk h r
  | Prevote (pk, h, r, id, _) ->
     (match id with
      | Some hash ->
         Format.fprintf ppf "[%a] Prevote [%d:%d] = %a" Ed25519.Public_key.pp pk h r Blake2B.pp (Blake2B.of_bytes_exn hash)
      | None ->
         Format.fprintf ppf "[%a] Prevote [%d:%d] = nil" Ed25519.Public_key.pp pk h r)
  | Precommit (pk, h, r, id, _) ->
     (match id with
      | Some hash ->
         Format.fprintf ppf "[%a] Precommit [%d:%d] = %a" Ed25519.Public_key.pp pk h r Blake2B.pp (Blake2B.of_bytes_exn hash)
      | None ->
         Format.fprintf ppf "[%a] Precommit [%d:%d] = nil" Ed25519.Public_key.pp pk h r)
