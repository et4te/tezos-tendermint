(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Error_monad
open Gossip

(* Constants *)

let n = 10
let f = n / 3

let prevote_threshold = 2 * f + 1
let precommit_threshold = 2 * f + 1
let superior_rounds_threshold = f + 1

let timeout_propose_multiplier = 0.1
let timeout_prevote_multiplier = 0.1
let timeout_precommit_multiplier = 0.1

let round_max = 1024
let decisions_max = 1024

(* Alias Types *)

type height = int
type round = int

(* Types *)

type t = {
    public_key : Ed25519.Public_key.t;
    secret_key : Ed25519.Secret_key.t;
    mutable height : height;
    mutable round : round;
    mutable step : Step.t;
    mutable locked_value : Value.t option;
    mutable locked_round : round;
    mutable valid_value : Value.t option;
    mutable valid_round : round;
    mutable first_proposal : bool;
    mutable first_prevote : bool;
    mutable first_precommit : bool;
    mutable gossip_set : Gossip_set.t;
    proposers : (height * round, Ed25519.Public_key.t) Hashtbl.t;
    decisions : (height, Value.t) Hashtbl.t;
    broadcast_delay : float;
    broadcast_pipe : Gossip.t Lwt_pipe.t;
    event_pipe : (Step.t * Step.t) Lwt_pipe.t;
  }

let create public_key secret_key proposers broadcast_delay = {
    public_key;
    secret_key;
    height = 0;
    round = 0;
    step = Step.Propose;
    locked_value = None;
    locked_round = -1;
    valid_value = None;
    valid_round = -1;
    first_proposal = true;
    first_prevote = true;
    first_precommit = true;
    gossip_set = Gossip_set.empty;
    proposers;
    decisions = Hashtbl.create decisions_max;
    broadcast_delay;
    broadcast_pipe = Lwt_pipe.create ();
    event_pipe = Lwt_pipe.create ();
  }

let proposer t h r =
  Hashtbl.find t.proposers (h, r)

let broadcast t g =
  Lwt_unix.sleep t.broadcast_delay >>= fun () ->
  Lwt_pipe.push t.broadcast_pipe g

let transition t step =
  let previous_step = t.step in
  t.step <- step;
  Lwt_pipe.push t.event_pipe (previous_step, t.step)

(** Rules *)

(* Timeouts - Part 1 *)

let rec soft_exp x =
  if x <= 2. then
    1.
  else
    soft_exp (x -. 2.) +. soft_exp (x -. 3.)

let timeout_propose r =
  soft_exp (float_of_int r) *. timeout_propose_multiplier

let on_timeout_propose t h r =
  if t.height = h && t.round = r && t.step = Step.Propose then
    broadcast t (Prevote (t.public_key, t.height, t.round, None, None)) >>= fun _ ->
    transition t Step.Prevote
  else
    Lwt.return ()

(* Start *)

let start_round t round =
  t.first_proposal <- true;
  t.first_prevote <- true;
  t.first_precommit <- true;
  t.round <- round;
  transition t Step.Propose >>= fun () ->
  if proposer t t.height t.round = t.public_key then
    match t.valid_value with
    | Some _ ->
       broadcast t (Proposal (t.public_key, t.height, t.round, t.valid_value, t.valid_round))
    | None ->
       let v = Value.generate t.secret_key in
       broadcast t (Proposal (t.public_key, t.height, t.round, Some v, t.valid_round))
  else
    begin
      Lwt.async(fun () ->
          Lwt_unix.sleep (timeout_propose t.round) >>= fun () ->
          on_timeout_propose t t.height t.round
        );
      Lwt.return ()
    end

let start t =
  start_round t 0

(* Timeouts - Part 2 *)

let timeout_prevote r =
  soft_exp (float_of_int r) *. timeout_prevote_multiplier

let timeout_precommit r =
  soft_exp (float_of_int r) *. timeout_precommit_multiplier

let on_timeout_prevote t h r =
  if t.height = h && t.round = r && t.step = Step.Prevote then
    broadcast t (Precommit (t.public_key, t.height, t.round, None, None)) >>= fun _ ->
    transition t Step.Precommit
  else
    Lwt.return ()

let on_timeout_precommit t h r =
  if t.height = h && t.round = r then
    start_round t (t.round + 1)
  else
    Lwt.return ()

(* L22 *)

let upon_valid_proposal t =
  let rec loop gossip = match gossip with
    | [] ->
       Lwt.return ()
    | Proposal (pk, h, r, v, vr) :: tl ->
       if t.step = Step.Propose then
         if proposer t t.height t.round = pk then
           if vr = -1 && t.height = h && t.round = r then
             (if Value.valid pk v && (t.locked_round = -1 || t.locked_value = v) then
                begin
                  Value.hash v >>= function
                  | Ok hash ->
                     let s = Ed25519.sign t.secret_key hash in
                     broadcast t (Prevote (t.public_key, t.height, t.round, Some hash, Some s))
                  | Error _ ->
                     broadcast t (Prevote (t.public_key, t.height, t.round, None, None))
                end
              else
                broadcast t (Prevote (t.public_key, t.height, t.round, None, None)))
             >>= fun () ->
             transition t Step.Prevote
           else
             loop tl
         else
           loop tl
       else
         loop tl
    | _ :: tl ->
       loop tl
  in loop (Gossip_set.elements t.gossip_set)
    
(* L28 *)

let upon_valid_prevotes t vr v =
  let rec loop gossip threshold = match gossip with
    | [] ->
       false
    | Prevote (_, h, r, id, _) :: tl ->
       if h = t.height && r = vr && id = Some v then
         if threshold + 1 >= prevote_threshold then
           true
         else
           loop tl (threshold + 1)
       else
         loop tl threshold
    | _ :: tl ->
       loop tl threshold
  in
  loop (Gossip_set.elements t.gossip_set) 0

let upon_proposal t =
  let rec loop gossip = match gossip with
    | [] ->
       Lwt.return ()
    | Proposal (pk, h, r, v, vr) :: tl ->
       if h = t.height && r = t.round then
         if proposer t t.height t.round = pk then
           begin
             Value.hash v >>= function
             | Ok hash ->
                if upon_valid_prevotes t vr hash then
                  if t.step = Step.Propose && (vr >= 0 && vr < t.round) then
                    (if Value.valid pk v && (t.locked_round <= vr || t.locked_value = v) then
                       begin
                         let s = Ed25519.sign t.secret_key hash in
                         broadcast t (Prevote (t.public_key, t.height, t.round, Some hash, Some s))
                       end
                     else
                       broadcast t (Prevote (t.public_key, t.height, t.round, None, None)))
                    >>= fun () ->
                    transition t Step.Prevote
                  else
                    loop tl
                else
                  loop tl
             | Error _ -> (* FIXME: we ignore the error :/ *)
                loop tl
           end
         else
           loop tl
       else
         loop tl
    | _ :: tl ->
       loop tl
  in loop (Gossip_set.elements t.gossip_set)

(* L34 *)

let upon_prevote_once t =
  let rec loop gossip threshold = match gossip with
    | [] ->
       Lwt.return ()
    | Prevote (_pk, h, r, _, _) :: tl ->
       if t.step = Step.Prevote then
         if t.height = h && t.round = r then
           if threshold + 1 >= prevote_threshold then
             begin
               t.first_prevote <- false;
               Lwt.async(fun () ->
                   Lwt_unix.sleep (timeout_prevote t.round) >>= fun () ->
                   on_timeout_prevote t t.height t.round
                 );
               Lwt.return ()
             end
           else
             loop tl (threshold + 1)
         else
           loop tl threshold
       else
         loop tl threshold
    | _ :: tl ->
       loop tl threshold
  in
  if t.first_prevote then
    loop (Gossip_set.elements t.gossip_set) 0
  else
    Lwt.return ()

(* L36 *)

let upon_proposal_and_prevote t =
  let rec loop gossip = match gossip with
    | [] ->
       Lwt.return ()
    | Proposal (pk, h, r, v, _vr) :: tl ->
       if t.height = h && t.round = r then
         if proposer t t.height t.round = pk then
           if Value.valid pk v && (t.step = Step.Prevote || t.step = Step.Precommit) then
             begin
               Value.hash v >>= function
               | Ok hash ->
                  if upon_valid_prevotes t t.round hash then
                    begin
                      t.first_proposal <- false;
                      (if t.step = Step.Prevote then
                         begin
                           t.locked_value <- v;
                           t.locked_round <- t.round;
                           let s = Ed25519.sign t.secret_key hash in
                           broadcast t (Precommit (t.public_key, h, r, Some hash, Some s))
                           >>= fun () ->
                           transition t Step.Precommit
                         end
                       else
                         Lwt.return ()
                      ) >>= fun () ->
                      t.valid_value <- v;
                      t.valid_round <- r;
                      Lwt.return ()
                    end 
                  else
                    loop tl
               | Error _ ->
                  loop tl
             end
           else
             loop tl
         else
           loop tl
       else
         loop tl
    | _ :: tl->
       loop tl
  in if t.first_proposal then
       loop (Gossip_set.elements t.gossip_set)
     else
       Lwt.return ()

(* L44 *)

let upon_nil_prevotes t =
  let rec loop gossip threshold = match gossip with
    | [] ->
       Lwt.return ()
    | Prevote (_pk, h, r, None, _s) :: tl ->
       if h = t.height && r = t.round then
         if threshold + 1 >= prevote_threshold then
           broadcast t (Precommit (t.public_key, t.height, t.round, None, None))
           >>= fun () ->
           transition t Step.Precommit
         else
           loop tl (threshold + 1)
       else
         loop tl threshold
    | _ :: tl ->
       loop tl threshold
  in
  if t.step = Step.Prevote then
    loop (Gossip_set.elements t.gossip_set) 0
  else
    Lwt.return ()

(* L47 *)

let upon_precommit_once t =
  let rec loop gossip threshold = match gossip with
    | [] ->
       Lwt.return ()
    | Precommit (_pk, h, r, _id, _s) :: tl ->
       if h = t.height && r = t.round then
         if threshold + 1 >= precommit_threshold then
           begin
             t.first_precommit <- false;
             Lwt.async(fun () ->
                 Lwt_unix.sleep (timeout_precommit t.round) >>= fun () ->
                 on_timeout_precommit t t.height t.round
               );
             Lwt.return ()
           end
         else
           loop tl (threshold + 1)
       else
         loop tl threshold
    | _ :: tl ->
       loop tl threshold
  in
  if t.first_precommit then
    loop (Gossip_set.elements t.gossip_set) 0
  else
    Lwt.return ()

(* L49 *)

let upon_valid_precommits t h1 r1 id1 =
  let rec loop gossip threshold = match gossip with
    | [] ->
       false
    | Precommit (_pk, h, r, id, _s) :: tl ->
       if h = h1 && r = r1 && id = id1 then
         if threshold + 1 >= precommit_threshold then
           true
         else
           loop tl (threshold + 1)
       else
         loop tl threshold
    | _ :: tl ->
       loop tl threshold
  in
  loop (Gossip_set.elements t.gossip_set) 0

let upon_proposal_and_precommit t =
  let older gossip new_height =
    match gossip with
    | Proposal (_, h, _, _, _)
    | Prevote (_, h, _, _, _)
    | Precommit (_, h, _, _, _) ->
       h >= new_height
  in
  let rec loop gossip = match gossip with
    | [] ->
       Lwt.return ()
    | Proposal (pk, h, r, v, _vr) :: tl ->
       if proposer t t.height r = pk then
         begin
           Value.hash v >>= function
           | Ok hash ->
              if upon_valid_precommits t t.height r (Some hash) then
                if Hashtbl.find_opt t.decisions h = None then
                  if Value.valid pk v then
                    begin
                      Hashtbl.add t.decisions t.height (Value.get_exn v);
                      t.height <- t.height + 1;
                      t.locked_value <- None;
                      t.locked_round <- -1;
                      t.valid_value <- None;
                      t.valid_round <- -1;
                      t.gossip_set <- Gossip_set.filter (fun g -> older g t.height) t.gossip_set;
                      t.first_proposal <- true;
                      t.first_prevote <- true;
                      t.first_precommit <- true;
                      start_round t 0
                    end
                  else
                    loop tl
                else
                  loop tl
              else
                loop tl
           | Error _ ->
              loop tl
         end
       else
         loop tl
    | _ :: tl ->
       loop tl
  in loop (Gossip_set.elements t.gossip_set)

(* L55 *)

let upon_superior_rounds t =
  let rec loop gossip saved = match gossip with
    | [] ->
       Lwt.return ()
    | Proposal (_, h, r, _, _) :: tl
    | Prevote (_, h, r, _, _) :: tl
    | Precommit (_, h, r, _, _) :: tl ->
       if t.height = h then
         if r > t.round then
           (match Hashtbl.find_opt saved r with
            | Some count ->
               if count + 1 >= superior_rounds_threshold then
                 start_round t r
               else
                 begin
                   Hashtbl.replace saved r (count + 1);
                   loop tl saved
                 end
            | None ->
               let _ = Hashtbl.add saved r 1 in
               loop tl saved)
         else
           loop tl saved
       else
         loop tl saved
  in loop (Gossip_set.elements t.gossip_set) (Hashtbl.create round_max)

let trigger_rules t =
  (* L22 *)
  upon_valid_proposal t >>= fun () ->
  (* L28 *)
  upon_proposal t >>= fun () ->
  (* L34 *)
  upon_prevote_once t >>= fun () ->
  (* L36 *)
  upon_proposal_and_prevote t >>= fun () ->
  (* L44 *)
  upon_nil_prevotes t >>= fun () ->
  (* L47 *)
  upon_precommit_once t >>= fun () ->
  (* L49 *)
  upon_proposal_and_precommit t >>= fun () ->
  (* L55 *)
  upon_superior_rounds t
