(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* Constants *)

val n : int
(** The number of nodes in the network. *)

val f : int
(** The amount of fault tolerance. *)

val prevote_threshold : int
(** The amount of prevotes required to progress to the Precommit step. *)

val precommit_threshold : int
(** The amount of precommits required to converge on a block. *)

val superior_rounds_threshold : int
(** The number of superior rounds from other nodes required in order for this node to
    progress to the next round. 
 *)

(* Timeouts in Tendermint are used to ensure progress occur within the protocol. 
   e.g. If there is less than 2/3 prevotes before the prevote timeout, a callback
   is called to force a transition to the next state whilst voting 'nil'.   
 *)

val timeout_propose_multiplier : float
(** The timeout multiplier for waiting on a proposal. *)

val timeout_prevote_multiplier : float
(** The timeout multiplier for waiting on prevotes. *)

val timeout_precommit_multiplier : float
(** The timeout multiplier for waiting on precommits. *)

val round_max : int
(** The maximum amount of rounds supported by the rounds hash table. By default
    this is 1024 which should be largely sufficient given that the timeouts are
    computed according to the rounds.
 *)

val decisions_max : int
(** The maximum amount of consensus convergence that the decision hash table 
    supports. This should be replaced by a pipe or something infinite.
 *)

(* Alias Types *)

type height = int
type round = int

type t = {
    public_key : Ed25519.Public_key.t;
    (** Each node has a public key which identifies gossip messages sent by it. *)
    secret_key : Ed25519.Secret_key.t;
    (** Each node has a secret key used to sign messages. *)
    mutable height : height;
    (** The current consensus instance. *)
    mutable round : round;
    (** The current round within a consensus instance. *)
    mutable step : Step.t;
    (** The current step that this consensus instance is at (e.g Step.Propose) *)
    mutable locked_value : Value.t option;
    (** Value which gets locked within rule L36 in order to ensure progress. *)
    mutable locked_round : round;
    (** Round which gets locked within rule L36 in order to ensure progress. *)
    mutable valid_value : Value.t option;
    mutable valid_round : round;
    mutable first_proposal : bool;
    (** Ensures that values only get locked once. *)
    mutable first_prevote : bool;
    (** Ensures that arbitrary prevotes (those with * values) are only accepted once. *)
    mutable first_precommit : bool;
    (** Ensures that arbitrary precommits (those with * values) are only accepted once. *)
    mutable gossip_set : Gossip_set.t;
    (** The set of gossip messages which the node uses to trigger the atomic rules. *)
    proposers : (height * round, Ed25519.Public_key.t) Hashtbl.t;
    (** The list of proposers organised by height * round. *)
    decisions : (height, Value.t) Hashtbl.t;
    (** Successful consensus decisions. *)
    broadcast_delay : float;
    (** Artificial delay on communication channels between nodes (for testing). *)
    broadcast_pipe : Gossip.t Lwt_pipe.t;
    (** A pipe which gets consumed by all other nodes. This is in order to replicate 
        a perfectly connected mesh within the tests. *)
    event_pipe : (Step.t * Step.t) Lwt_pipe.t;
    (** A pipe which can be used to propagate events (e.g state transitions). *)
  }

val create :
  Ed25519.Public_key.t ->
  Ed25519.Secret_key.t ->
  (height * round, Ed25519.Public_key.t) Hashtbl.t ->
  float ->
  t

(* Timeouts *)

val soft_exp : float -> float
(** The soft exponent exponential backoff function. Used in computing timeouts from 
    rounds. *)

val timeout_propose : round -> float
val timeout_prevote : round -> float
val timeout_precommit : round -> float

(* Gossip *)

val broadcast : t -> Gossip.t -> unit Lwt.t

(* Rounds *)

val start : t -> unit Lwt.t

val start_round : t -> round -> unit Lwt.t

(* Rules *)

(* L22 *)

val upon_valid_proposal : t -> unit Lwt.t

(* L28 *)

val upon_valid_prevotes : t -> round -> Value.block_hash -> bool

val upon_proposal : t -> unit Lwt.t

(* L34 *)

val upon_prevote_once : t -> unit Lwt.t

(* L36 *)

val upon_proposal_and_prevote : t -> unit Lwt.t

(* L44 *)

val upon_nil_prevotes : t -> unit Lwt.t

(* L47 *)

val upon_precommit_once : t -> unit Lwt.t

(* L49 *)

val upon_valid_precommits : t -> height -> round -> Value.block_hash option -> bool

val upon_proposal_and_precommit : t -> unit Lwt.t

(* L55 *)

val upon_superior_rounds : t -> unit Lwt.t

val trigger_rules : t -> unit Lwt.t
(** Triggers all the rules sequentially. *)
