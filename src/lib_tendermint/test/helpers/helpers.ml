(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tendermint
open Lwt

(* Tendermint state construction *)

let generate_keys lim =
  Array.init lim @@ fun _ ->
    let (_, pk, sk) = Ed25519.generate_key () in
    (pk, sk)

let generate_schedule keys height_lim round_lim =
  let schedule = Hashtbl.create height_lim in
  for height = 0 to height_lim - 1 do
    for round = 0 to round_lim - 1 do
      let (public_key, _) = keys.(height) in
      Hashtbl.add schedule (height, round) public_key;
    done;
  done;
  schedule

let start_tendermint (pk, sk) schedule broadcast_delay =
  let state = create pk sk schedule broadcast_delay in
  async(fun () -> start state);
  state

let start_tendermint_instances keys schedule n delay =
  let rec construct i states =
    if i >= n then
      states
    else
      construct (i + 1) (start_tendermint keys.(i) schedule delay :: states)
  in construct 0 []

let start_delayed_instances keys schedule n delays =
  let rec construct i states =
    if i >= n then
      states
    else
      construct (i + 1) (start_tendermint keys.(i) schedule (List.nth delays i) :: states)
  in construct 0 []

(* State merging test utilities *)

let drain pipe =
  if Lwt_pipe.is_empty pipe then
    Lwt.return []
  else
    Lwt_pipe.pop_all pipe >>= fun list ->
    Lwt.return list

let merge nodes =
  Lwt_list.fold_right_s (fun node gossip_list ->
      drain node.broadcast_pipe >>= fun l ->
      Lwt.return (List.append l gossip_list)
    ) nodes [] >>= fun gossip_list ->
  let u = Gossip_set.of_list gossip_list in
  List.iter (fun t ->
      t.gossip_set <- Gossip_set.union t.gossip_set u
    ) nodes;
  Lwt.return ()

(* Format / Printing to test log *)

let print_gossip_set state =
  Gossip_set.iter (fun gossip ->
      let s = Format.asprintf "%a\n" Gossip.pp gossip in
      Printf.printf "%s" s
    ) state.gossip_set

let print_transition transition =
  let s = Format.asprintf "%a\n" Transition.pp transition in
  Printf.printf "%s" s

let print_gossip states =
  List.iter (fun state ->
      print_gossip_set state
    ) states

let print_events states =
  Lwt_list.map_p (fun state ->
      drain state.event_pipe
    ) states >>= fun events ->
  List.iter (fun t -> print_transition t) (List.concat events);
  Lwt.return ()

(* Tendermint state progression *)

let tick states consensus_delay =
  merge states >>= fun () ->
  print_gossip states;
  Lwt_list.iter_p (fun state ->
      trigger_rules state
    ) states >>= fun () ->
  Lwt_unix.sleep consensus_delay

let tick_lim states lim consensus_delay =
  let rec loop i =
    if i >= lim then
      Lwt.return ()
    else
      tick states consensus_delay >>= fun () ->
      loop (i + 1)
  in loop 0

