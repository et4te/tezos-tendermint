(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tendermint
open Lwt

let lim = 10  

let test_ideal_convergence _switch () =
  let keys = Helpers.generate_keys lim in
  let proposers = Helpers.generate_schedule keys lim 5 in
  let nodes = Helpers.start_tendermint_instances keys proposers lim 0.01 in

  Helpers.tick_lim nodes 5 0.05 >>= fun () ->

  let height = 0 in
  let node_0 = List.nth nodes 0 in
  let value_0 = Hashtbl.find node_0.decisions height in
  let testable = Alcotest.testable Value.pp Value.(=) in
  for i = 1 to (lim - 1) do
    let node = List.nth nodes i in
    let value = Hashtbl.find node.decisions height in
    Alcotest.check testable "value" value_0 value
  done;
  Lwt.return ()

let test_required_convergence _switch () =
  let keys = Helpers.generate_keys lim in
  let proposers = Helpers.generate_schedule keys lim 5 in
  let nodes = Helpers.start_delayed_instances keys proposers lim
                [0.01; 0.01; 0.01; 0.01; 0.01; 0.01; 0.01; 1.0; 1.0; 1.0]
  in

  Helpers.tick_lim nodes 10 0.05 >>= fun () ->

  let height = 0 in
  let node_0 = List.nth nodes 0 in
  let value_0 = Hashtbl.find node_0.decisions height in
  let testable = Alcotest.testable Value.pp Value.(=) in
  for i = 1 to 7 do
    let node = List.nth nodes i in
    let value = Hashtbl.find node.decisions height in
    Alcotest.check testable "value" value_0 value
  done;
  Lwt.return ()

let test_eventual_convergence _switch () =
  let keys = Helpers.generate_keys lim in
  let proposers = Helpers.generate_schedule keys lim 5 in
  let nodes = Helpers.start_delayed_instances keys proposers lim
                [0.01; 0.01; 0.01; 1.0; 1.0; 1.0; 1.0; 1.0; 1.0; 1.0]
  in

  Helpers.tick_lim nodes 10 0.05 >>= fun () ->

  let height = 0 in
  let node_0 = List.nth nodes 0 in
  let value_0 = Hashtbl.find node_0.decisions height in
  let testable = Alcotest.testable Value.pp Value.(=) in
  for i = 1 to lim - 1 do
    let node = List.nth nodes i in
    let value = Hashtbl.find node.decisions height in
    Alcotest.check testable "value" value_0 value
  done;
  Lwt.return ()

let test_slow_network _switch () =
  let keys = Helpers.generate_keys lim in
  let proposers = Helpers.generate_schedule keys lim 5 in
  let nodes = Helpers.start_delayed_instances keys proposers lim
                [1.0; 1.0; 1.0; 1.0; 1.0; 1.0; 1.0; 1.0; 1.0; 1.0]
  in

  Helpers.tick_lim nodes 30 0.05 >>= fun () ->

  let height = 0 in
  let node_0 = List.nth nodes 0 in
  let value_0 = Hashtbl.find node_0.decisions height in
  let testable = Alcotest.testable Value.pp Value.(=) in
  for i = 1 to lim - 1 do
    let node = List.nth nodes i in
    let value = Hashtbl.find node.decisions height in
    Alcotest.check testable "value" value_0 value
  done;
  Lwt.return ()

let test_set = [
    Alcotest_lwt.test_case "test.ideal" `Quick test_ideal_convergence;
    Alcotest_lwt.test_case "test.2f + 1" `Quick test_required_convergence;
    Alcotest_lwt.test_case "test.f" `Quick test_eventual_convergence;
    Alcotest_lwt.test_case "test.10x_delay" `Slow test_slow_network;
  ]

let () =
  Alcotest.run "tezos-tendermint-model" [
      "test_set", test_set;
    ]

