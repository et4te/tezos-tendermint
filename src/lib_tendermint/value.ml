(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Error_monad

type error +=
   | Invalid_block

(* Error_monad.register_error_kind
 *    `Temporary
 *    ~id:"hash.invalid_block"
 *    ~title:"Block was None"
 *    ~description:"Attempt to hash an empty value"
 *    ~pp:(fun ppf () -> Format.fprintf ppf "Invalid block in hash")
 *    block_encoding *)

type block = { fake : int32 list }
type block_hash = MBytes.t

let block_encoding =
  let open Data_encoding in
  conv
    (fun ({ fake }) -> (fake))
    (fun (fake) -> { fake })
    (obj1 (req "fake" (list int32)))

let sign_block secret_key block =
  let open Data_encoding in
  let mbytes = Binary.to_bytes_exn block_encoding block in
  Ed25519.sign secret_key mbytes

type t = Signed_block of block * Ed25519.t

let (=) a b = a = b

let pp ppf _t =
  Format.fprintf ppf "#<signed-block>"

exception No_value

let get_exn = function
  | Some v ->
     v
  | None ->
     raise No_value

let generate secret_key =
  (* TODO: Generate a proper block *)
  let r = [ Random.int32 (Int32.of_int 255) ] in
  let s = sign_block secret_key { fake = r } in
  Signed_block ({ fake = r }, s)

let valid pk = function
  | Some (Signed_block (block, signature)) ->
     let mbytes = Data_encoding.Binary.to_bytes_exn block_encoding block in
     Ed25519.check pk signature mbytes
  | None ->
     false
  
let hash = function
  | Some (Signed_block (block, _)) ->
     let mbytes = Data_encoding.Binary.to_bytes_exn block_encoding block in
     return (Blake2B.to_bytes @@ Blake2B.hash_bytes [mbytes])
  | None ->
     fail Invalid_block
